#!/usr/bin/env python
import numpy as np

# Output current
Iout = 1
# range of LNB output voltages
Vlnb = np.array([13.4, 19.4])
# Input voltage
Vin = 7
# inductance value
L = 10e-6
# switching frequency
fsw = 1e6

D = 1 - Vin/(Vlnb + 0.8)
print(f"D: {D}")

Ipeak = Iout/(1-D) + 0.5*Vin*D/(L*fsw)
print(f"Ipeak: {Ipeak}")
