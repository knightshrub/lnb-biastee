# LNB bias tee

This is a DiSEqC1.x compatible bias tee for universal ("Astra") LNBs which are
commonly used in Europe to receive FTA TV channels.

The design is based on a [Texas Instruments TPS652353](https://www.ti.com/lit/ds/symlink/tps652353.pdf)
regulator designed specifically for this application.

Using the on-board DIP switch the different reception parameters of the LNB
can be selected:

- LO Frequency: 9.75 or 10.6 GHz
- Receive polarization: vertical or horizontal
- LNB supply voltage: 18.2/13.4 V or 19.4/14.6 V

Check `lnb-biastee.pdf` for the schematic in PDF format.

![](lnb-biastee.jpg)

## QO-100 reception

This bias tee was designed with the intention of receiving emissions from the
[QO-100 amateur radio satellite](https://amsat-dl.org/eshail-2-amsat-phase-4-a-qatar-oscar-100/)
10 GHz transponder downlink.

### Narrow band transponder (NB)

Analog modulation schemes like SSB

- LO frequency: 9.75 GHz
- Polarization: vertical
- RF frequency: 10489.5 to 10490.0 MHz
- IF frequency: 739.5 to 740 MHz

### Wide band transponder (WB)

Digital amateur TV using DVB-S2

- LO frequency: 9.75 GHz
- Polarization: horizontal
- RF frequency: 10491.0 to 10499.0 MHz
- IF frequency: 741.0 to 749 MHz

# License

This project is open hardware. The [KiCAD](https://www.kicad.org/) design files are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) License.

[![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)
